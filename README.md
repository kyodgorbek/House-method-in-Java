# House-method-in-Java



import java.awt.*;
import java.applet.Applet;


public class House extends Applet {
  // Data fields
  // Define 4 corner points for the house
  private int x1 = 100; // lower-left
  private int y1 = 200; // corner of roof
  private int x2 = 300; // peak of roof
  private int y2 = 100; //
  private int x3 = 500; // lower-right
  private int y3 = 200; // corner roo of roof
  private int x4 = 500; // bottom-right
  private int y4 = 400; // corner of house
  
  // Corner points for door
  private int x5 = 275; // top-left
  private int y5 = 325; // corner of door
  private int x6 = 325; // bottom-right
  private int y6 = 400;
  
  public void paint(Graphics g){
   
   g.setColor(Color.black);
   
   //Draw the roof
   g.drawLine(x1, y1, x2, y2);
   g.drawLine(x2, y2, x3, y3);
   
   // Draw the house as a box.
   g.drawRect(x1, y1, x4 - x1, y4 - y1);
   
   //Draw  a door
   g.setColor(Color.green);
   g.fillRect(x5, y5, x6 - x5, y6 - y5);
   g.setColor(Color.black);
  
          //Label the cornet points
          g.drawString(" (x1,y1)", x1, y1);
          g.drawString(" (x2,y2)", x2, y2);
          g.drawString(" (x3,y3)", x3, y3);
          g.drawString(" (x4,y4)", x4, y4);
          g.drawString(" (x5,y5)", x5, y5);
          g.drawString(" (x6,y6)", x6, y6);
      
        
          
  
  
  }
 }  
